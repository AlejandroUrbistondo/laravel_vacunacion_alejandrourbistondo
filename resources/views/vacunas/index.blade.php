@extends('layouts.master')
 
@section('titulo')
  Vacunas
@endsection 
 
@section('contenido')
  <div class="row">
    @foreach( $vacunas as $vacuna )
      <div class="card m-1 bg-light border-secondary" style="width: 18rem;">        
        <div class="card-body">
            <a href="{{ route("vacunas.show", $vacuna) }}"><h3 class="card-title">{{$vacuna->nombre}}</h3></a>
            <p><b> Posibles grupos de vacunacion: </b></p>
            <div class="row">
                <ul>
                @foreach($vacuna->grupos as $grupo)
                    <li>{{$grupo->nombre}}</li>
                @endforeach
                </ul>
            </div>
        </div>
      </div>
    @endforeach
  </div>
@endsection 