@extends('layouts.master') 
 
@section('titulo')
  Mostrar vacuna
@endsection 
 
@section('contenido')
  @if (session("mensaje"))
      <h3 class="alert-warning">{{ session("mensaje") }}</h3>
  @endif
  <div class="row">  
    <div class="col-sm-9">
        <h1>{{ $vacuna->nombre}}</h1>
        <h2>Pacientes NO VACUNADOS: </h2>
        <table>
            <tr>
                <th>Nombre</th>
                <th>Grupo de vacunación</th>
                <th>Prioridad</th>
                <th>Acción</th>
            </tr>
            @if(count($vacuna->grupos) > 0)            
                @foreach($vacuna->gruposPrioridad as $grupo)
                    @if(count($grupo->pacientes) > 0)
                        @foreach($grupo->pacientes as $paciente)
                            <tr>
                            @if(!$paciente->vacunado)
                                <td>{{ $paciente->nombre }}</td>
                                <td>{{ $paciente->grupo->nombre }}</td>
                                <td>{{ $paciente->grupo->prioridad }}</td>
                                <td><a class="btn btn-primary" href="{{ route("pacientes.vacunar", $paciente) }}" role="button" id="vacunar" name="vacunar">Vacunar</a></td>
                            @endif
                            </tr>
                        @endforeach
                        
                    @endif
                @endforeach
                
            @endif
        </table>

        <h2>Pacientes vacunados: </h2>
        <div class="row">
        @if(count($vacuna->grupos) > 0)
            @foreach( $vacuna->grupos as $grupo )
              <div class="card m-1 bg-light border-secondary" style="width: 18rem;">        
                <div class="card-body">                    
                    <h4>{{ $grupo->nombre }}</h4>
                    <div class="row">
                        @if(count($grupo->pacientes) > 0)
                            @foreach($grupo->pacientes as $paciente)
                                @if($paciente->vacunado)
                                    <ul>                                                        
                                        <li>{{$paciente->nombre}} ({{ $paciente->fechaVacuna }})</li>                                
                                    </ul>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
              </div>
            @endforeach
        @endif
        </div>
@endsection 