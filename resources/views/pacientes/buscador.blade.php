@extends('layouts.master')
 
@section('titulo')
  Buscador
@endsection 
 
@section('contenido')

<form class="d-flex">
    <input id="busqueda" class="form-control mr-sm-3" type="text" placeholder="Buscar" aria-label="Buscar">
</form>

<script>
$(document).ready(function () {
    $("#busqueda").autocomplete({
        source: function( request, response ) {
            $.ajax( {
                type: "POST",
                url: "{{url("pacientes/busquedaAjax")}}",
                dataType: "json",
                data: {
                    "_token": "{{csrf_token()}}",
                    "busqueda": request.term
            },
            success: function( data ) {
                response( data );
            }
        });
        }
    })
})
</script>
@endsection