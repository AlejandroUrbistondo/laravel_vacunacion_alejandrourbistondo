<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use Illuminate\Http\Request;

class PacienteController extends Controller
{
    public function vacunar(Paciente $paciente)
    {
        $paciente->vacunado = true;
        $paciente->fechaVacuna = date("y-m-d");
        $paciente->save();
        return back()->with("mensaje", "El paciente " . $paciente->nombre . " se ha vacunado");
    }

    public function buscador()
    {
        return view("pacientes.buscador");
    }

    public function search()
    {
        $pacientes = Paciente::all();
        return response()->json($pacientes);
    }
}
