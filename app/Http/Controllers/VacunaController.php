<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use App\Models\Vacuna;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class VacunaController extends Controller
{
    public function index()
    {
        $vacunas = Vacuna::all();
        return view("vacunas.index", compact("vacunas"));
    }

    public function show(Vacuna $vacuna)
    {
        return view("vacunas.show", compact("vacuna"));
    }

    public function crearVacuna(Request $request)
    {
        $vacuna = new Vacuna();
        $vacuna->nombre = $request->nombre;
        $vacuna->slug = Str::slug($request->nombre);
        $correcto = $vacuna->save();
        if($correcto)
            $mensaje = "La vacuna " . $request->nombre . " ha sido insertada correctamente";
        else
            $mensaje = "La vacuna " . $request->nombre . " no se ha posido insertar";

        return response()->json(['mensaje' => $mensaje]);
    }

    public function consultarVacunas(int $id)
    {
        $paciente = Paciente::query()->where("id", $id)->getModel();
        $vacunas = $paciente->grupo->vacunas;

        $mensaje = "<h1>Vacunas posibles para paciente con ID " . $id . "</h1><ul>";
        foreach ($vacunas as $vacuna) 
        {
            $mensaje .= "<li>" . $vacuna->nombre . "</li>";
        }
        $mensaje .= "</ul>";

        return response()->json(['mensaje' => $mensaje]);
    }
}
