<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacuna extends Model
{
    use HasFactory;

    public $table = "vacunas";

    public function grupos()
    {
        return $this->belongsToMany(Grupo::class);
    }

    public function gruposPrioridad()
    {
        return $this->belongsToMany(Grupo::class)->orderByDesc("prioridad");
    }

    public function getRouteKeyName()
    {
        return "slug";
    }
}
