<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGrupoVacunaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupo_vacuna', function (Blueprint $table) {
            $table->foreignId("grupo_id");
            $table->unsignedBigInteger("vacuna_id");

            $table->primary(["grupo_id", "vacuna_id"]);
            $table->foreign("grupo_id")->references("id")->on("grupos")->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign("vacuna_id")->references("id")->on("vacunas")->cascadeOnDelete()->cascadeOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupo_vacuna');
    }
}
