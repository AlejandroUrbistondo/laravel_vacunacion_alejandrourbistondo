<?php

namespace Database\Factories;

use App\Models\Grupo;
use App\Models\Paciente;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nombre = $this->faker->name;
        $total_grupos = count(Grupo::all());
        $vacunado = $this->faker->boolean;
        $fechaVacuna = null;
        if($vacunado)
            $fechaVacuna = $this->faker->dateTimeThisYear;

        return [
            "nombre" => $nombre,
            "slug" => Str::slug($nombre),
            'vacunado' => $vacunado,
            'grupo_id' => rand(1, $total_grupos),
            'fechaVacuna' => $fechaVacuna,
        ];
    }
}
