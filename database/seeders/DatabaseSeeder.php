<?php

namespace Database\Seeders;

use App\Models\Paciente;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        DB::table("vacunas")->delete();
        DB::table("grupos")->delete();
        DB::table("pacientes")->delete();
        
        $this->call(GrupoSeeder::class);
        $this->call(VacunaSeeder::class);        
        Paciente::factory(40)->create();
    }
}
